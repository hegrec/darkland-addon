AddCSLuaFile("autorun/sh_darkland.lua")


require("tmysql")
require("cmd")

tmysql.initialize("localhost", "roleplay", "x4!@2#", "gmodstuff", 3306, 2, 2)
DEVMODE = false

hook.Add("Initialize","SendLua",function()

	--add dua for the right folder
	local path 		= util.RelativePathToFull("cl.db")
	local server 	= string.sub(path,24,24)


	if server == "5" then DEVMODE = true end --don't add it on the dev server cause the spam is bad
		
	--old method
	--cmd.exec("copy C:\\SourceServers\\Server"..server.."\\orangebox\\garrysmod\\cache\\dua\\*.dua \"C:\\www\\garrysmod\\cache\\dua\\\"" )
	cmd.exec("duacopy.exe C:\\SourceServers\\Server"..server.."\\orangebox\\garrysmod\\cache\\dua\\ C:\\www\\garrysmod\\cache\\dua\\")
end)

function LoadMainStuff(pl,tbl,status,err)
	tbl = tbl[1]
	if !tbl then
		tbl = {}
		CreateAccount(pl)
	else
		local steamid = pl:SteamID()
		local sID = GAMEMODE.DarklandID or 0
		tmysql.query("UPDATE da_misc SET playername=\'"..tmysql.escape(pl:Name()).."\',lastlogin="..os.time()..",lastserver="..sID..",online=1 WHERE steamid=\'"..steamid.."\'")
	end
	pl:SetNWInt("m_Status",tonumber(tbl['membership']))
	pl.Loaded 			= true
	pl.AchievementVars = {}
	
	local t = string.Explode("|",tbl['AchievementVars'])
	for i,v in pairs(t) do
		local innerT = string.Explode(":",v)
		pl:SetDarklandVar(innerT[1],tonumber(innerT[2]))
	end
	hook.Call("DarklandProfileLoaded",GAMEMODE,pl)
	local steamid = pl:SteamID()
	timer.Create("saveAchVars_"..steamid,10,0,function()
		if !ValidEntity(pl) then timer.Destroy("saveAchVars_"..steamid) return end
		if pl.AchievementVars then
			local t = {}
			for i,v in pairs(pl.AchievementVars) do
				local s = i..":"..v
				table.insert(t,s)
			end
			local finalString = tmysql.escape(table.concat(t,"|"))
			tmysql.query("UPDATE da_misc SET AchievementVars='"..finalString.."' WHERE steamid='"..steamid.."'")
		end
	end)
end




function CreateAccount(pl)
	local steamid = pl:SteamID()
	local name = tmysql.escape(pl:Name())
	tmysql.query("INSERT INTO da_misc (steamid,playername,online,lastlogin,lastserver) VALUES (\'"..steamid.."\', \'"..name.."\',1,"..os.time()..",1)")
end

function LoadData(pl,steamid)
	pl:SetNWString("SteamID",steamid)
	tmysql.query("SELECT membership,AchievementVars FROM da_misc where steamid=\'"..steamid.."\'",function(res,status,err)
	
		if !ValidEntity(pl) then return end
		LoadMainStuff(pl,res) 
	end,1)
end
hook.Add("PlayerAuthed","LoadMainStuff",LoadData)


function FormatIP(ip)
	return string.sub(ip,1,string.find(ip,":")-1)
end

local meta = FindMetaTable("Player")

function meta:OriginalName()

	return self:GetNWString("OriginalAdminName","Admin Name Not Set")

end


------
--Remove online players if they are no longer online
------


function ValidateOnlinePlayers(tbl)
	local offlineSteamIDs = {}
	for i,v in pairs(tbl) do
		table.insert(offlineSteamIDs,v["steamid"])
	end
	local onlineIDs = {}
	for i,v in pairs(player.GetAll()) do
		onlineIDs[v:SteamID()] = 1
	end
	for i,v in pairs(offlineSteamIDs) do
		if onlineIDs[v] then offlineSteamIDs[i] = nil end
	end
	if table.Count(offlineSteamIDs) < 1 then return end
	
	local newTbl = {}
	for i,v in pairs(offlineSteamIDs) do
		table.insert(newTbl,"steamid='"..offlineSteamIDs[i].."'")
	end
	local steamIDString = table.concat(newTbl," or ")
	tmysql.query("UPDATE da_misc SET online=0 WHERE "..steamIDString,function(res,stat,err)print(err) end)
	
end
timer.Create("OnlinePlayerChecker",10,0,function() 
if DEVMODE then return end
local sID = GAMEMODE.DarklandID 
if !sID then return end 
tmysql.query("SELECT steamid FROM da_misc WHERE online=1 and lastserver="..sID,ValidateOnlinePlayers,1)
end)



-----------------------------
--BANS SYSTEM
---------------------------

function DoUnbans()
	tmysql.query("SELECT * FROM bans WHERE expiretime <> \"NEVER\"",function(res,stat,err)UnbanPlayers(res)end)
end
timer.Create("unbanPlayers",10,0,DoUnbans)
function UnbanPlayers(tbl)
	for i,v in pairs(tbl) do
		
		local expire = tonumber(v[4])
		if expire < os.time() then
			tmysql.query("DELETE FROM bans WHERE id="..tonumber(v[1]))
		end
	end		
end

function CheckBan(pl,tbl)
	if tbl[1] and tbl[1][1] then
		if tonumber(tbl[1][2]) then 
			game.ConsoleCommand("kickid "..pl:UserID().." Banned For "..nice_time(math.max(1,tonumber(tbl[1][2])-os.time())).."\n")
		else
			game.ConsoleCommand("kickid "..pl:UserID().." Banned Forever\n")
		end
	end
end
function LoadBans(pl,steamid) 

	tmysql.query("SELECT steamid,expiretime FROM bans where steamid=\'"..steamid.."\'",function(res,status,err) 
		if !ValidEntity(pl) then return end 
		CheckBan(pl,res) 
	end)
end
hook.Add("PlayerAuthed","DAM_BanCheck",LoadBans)


----------------------
--AFK SYSTEM
----------------------
local Time = 300 -- 5 mins 
function ResetTimer(ply)
	timer.Start("afkkick_"..ply:SteamID())
end

function CallHook(ply)
	if !ValidEntity(ply) || DEVMODE then return end
	timer.Destroy("afkkick_"..ply:SteamID())
	gamemode.Call("AFK",ply) --gamemode decides what to do
	--game.ConsoleCommand( Format( "kickid %i %s\n", ply:UserID(), "AFK for "..Time.." seconds" ) )
end

function NewAFKTimer(ply)
	timer.Create("afkkick_"..ply:SteamID(), Time, 1, CallHook, ply)
end

hook.Add("PlayerInitialSpawn","resettimerspawn",
	function(ply)
		timer.Create("afkkick_"..ply:SteamID(), Time, 1, CallHook, ply)
	end
)
hook.Add("KeyPress","resettimerkeyPress",ResetTimer)
hook.Add("PlayerSay","resettimerPlySay",ResetTimer)

-----------------
--NO SPAMJOIN
-------------------
local JoinIPS = {}
hook.Add("PlayerConnect","NoSpamJoin",function(name,ip)
	ip = FormatIP(ip)
	JoinIPS[ip] = JoinIPS[ip] or {joinAmt = 0,lastJoin = 0}

	if JoinIPS[ip].joinAmt > 5 then
		if CurTime()-JoinIPS[ip].lastJoin < 1 then
			game.ConsoleCommand("addip 2 "..ip.."\n")
			print("BANNED "..ip.." FOR SPAM JOINING LOL")
		else
			JoinIPS[ip].joinAmt = 0
		end
	end

	
	JoinIPS[ip].lastJoin = CurTime()
	JoinIPS[ip].joinAmt = JoinIPS[ip].joinAmt + 1
end
)


-------------------
--Serverside achievements
-------------------


function MySQLSaveAchievement(name,func,IconUrl,GameID,Desc,barFunc)
	local n = tmysql.escape(name)
	local i = tmysql.escape(IconUrl)
	local g = tonumber(GameID)
	local d = tmysql.escape(Desc)
	
	
	--print(("INSERT INTO achievement_list (GameID,IconURL,Name,Description) VALUES ("..g..",\'"..i.."\',\'"..n.."\',\'"..d.."\') ON DUPLICATE KEY UPDATE Name=Name"))
	tmysql.query("INSERT INTO achievement_list (GameID,IconURL,Name,Description) VALUES ("..g..",\'"..i.."\',\'"..n.."\',\'"..d.."\') ON DUPLICATE KEY UPDATE Name=Name")


end
hook.Add("OnAchievementAdded","MySQLSaveAchievement",MySQLSaveAchievement)
function meta:SetDarklandVar(var,val)
	if !self.AchievementVars then return end
	self.AchievementVars[var] = val
	umsg.Start("achievementVars",self)
		umsg.String(var)
		umsg.Long(val)
	umsg.End()
end

function meta:AddDarklandVar(var,amt)
	self.AchievementVars = self.AchievementVars or {}
	self.AchievementVars[var] = self.AchievementVars[var] or 0
	self.AchievementVars[var] = self.AchievementVars[var] + amt
	
	umsg.Start("achievementVars",self)
		umsg.String(var)
		umsg.Long(self.AchievementVars[var])
	umsg.End()

end


function meta:GetDarklandVar(var,other)
	other = other or 0
	if !self.AchievementVars then return other end
	return self.AchievementVars[var] or other;
end

timer.Create("IncreasePlayTime",1,0,
function() 
	for i,v in pairs(player.GetAll()) do 
		v:SetDarklandVar("playtime",v:GetDarklandVar("playtime",0)+1)
	end
end)

hook.Add("ShowSpare2","showAchievementMenu",
	function(pl)
		pl:ConCommand("show_achievements")
	end
)

timer.Create("SaveAchievementVars",10,0,function()
	for i,v in pairs(player.GetAll()) do

	end
end)

function VerifyAchievement(pl,cmd,args)

	local name = args[1]
	local t
	for i,v in pairs(achievement.GetAll()) do
		if v.Name == name then
			t = v
			break
		end
	end
	if !t.VerifyFunction(pl) then return end
	pl:EarnAchievement(t)


end
concommand.Add("~achievementEarned",VerifyAchievement)

function GiveAchievement(pl,name)


	local t
	for i,v in pairs(achievement.GetAll()) do
		if v.Name == name then
			t = v
			break
		end
	end
	pl:EarnAchievement(t)
end

function meta:HasAchievement(name)
	for i,v in pairs(self.AchievementsEarned) do
		if v == name then return true end
	end
	return false
end

function meta:EarnAchievement(t)
	if self:HasAchievement(t.Name) then return end
	table.insert(self.AchievementsEarned,t.Name)
	
	local name = tmysql.escape(t.Name)
	tmysql.query("INSERT INTO achievements_earned (steamid,achievementName) VALUES ('"..self:SteamID().."','"..name.."')")
	umsg.Start("playerEarnedAchievement",self)
		umsg.String(t.Name)
	umsg.End()
	hook.Call("PlayerEarnedAchievement",GAMEMODE,self,t)
end


function LoadAchievements(pl,tbl)
	if !ValidEntity(pl) then return end
	pl.AchievementsEarned = {}
	for i,v in pairs(tbl) do
		table.insert(pl.AchievementsEarned,v.achievementName)
		umsg.Start("getAchievement",pl)
			umsg.String(v.achievementName)
		umsg.End()
	end
end
hook.Add("PlayerAuthed","LoadPlayerAchievements",
function(pl,steamid)
local sID = GAMEMODE.DarklandID or 0
tmysql.query("SELECT e.achievementName from achievements_earned AS e,achievement_list AS l WHERE e.steamid='"..pl:SteamID().."' and l.Name=e.achievementName and (l.GameID=0 or l.GameID="..sID..")",function(res,s,e)LoadAchievements(pl,res)end,1)

end
)


------------------
--Scriptenforce verify
------------------
local SE
hook.Add("Initialize","SEVar",function()
SE = GetConVarString("sv_scriptenforcer")
end)
local function SendThis(ply)
ply:SendLua([[TC=timer.Create PR=pairs TN=tonumber RC=RunConsoleCommand]])
ply:SendLua([[local tab={{"sv_cheats",0,0},{"sv_scriptenforcer",]]..SE..[[,0}} TC("Timer.Think",.5,0,function() for k,v in PR(tab) do if(TN(LocalPlayer():GetInfo(v[1]))!=v[2]) then if(v[3]==1) then RC(v[1], v[2]) end RC("achievementRefresh") end end end)]])
end
hook.Add("PlayerInitialSpawn","NoHaxPlox",SendThis)

function LogMyAss(pl)

	filex.Append("hackerlist.txt",pl:SteamID().." - "..os.date().."\n")
	pl:ConCommand("disconnect")
end
concommand.Add("achievementRefresh",BanAndLogMyAss) --achievementRefresh sounds legit

